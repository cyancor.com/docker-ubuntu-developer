FROM ubuntu:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENV PATH="$PATH:/usr/games"

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --yes \
        cowsay \
        sudo \
        git \
        apt-utils \
        curl \
        wget \
        zip \
        unzip \
        bc \
        htop \
        nano \
        openssh-client \
        mono-complete \
        imagemagick \
        inkscape \
        php php-gd \
        software-properties-common \
        default-jre \
        default-jdk \
        net-tools \
        inetutils-ping \
        expect \
        # Python
        python3 \
        python3-distutils \
        python3-apt \
# NodeJS
    && curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash - \
    && apt-get install -y nodejs \
# Inkscape
    && mkdir -p /.config/inkscape \
    && chmod 0777 /.config/inkscape \
# Finalize
    && rm -rf /var/lib/apt/lists/* \
    && npm cache clear -f \
    && cowsay "Docker build finished."
